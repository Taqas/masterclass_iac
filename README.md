# masterclass_iac

Solution workshop for masterclass 2020.

## Prerequisities

- AWS account with access to
  - Cloudformation
  - Api Gateway
  - Lambda
  - DynamoDB
  - IAM

## Deploy

Change `lambda-apigateway-cf-<name>` to your name, ex `lambda-apigateway-cf-sjors`, for the parameter `DynamoDBTableName` in `cloudformation/apigateway.json`

```json
"Default" : "lambda-apigateway-cf-<name>"
```

Deploy
```bash
NAME=<name>
aws cloudformation deploy --stack-name api_$NAME --template-file cloudformation/apigateway.json \
--profile ff --capabilities CAPABILITY_NAMED_IAM
```

### Delete stack

```bash
aws cloudformation delete-stack --stack-name api_$NAME --profile ff
```

### Test deployment

```bash
URL=<url>
TABLENAME=<tablename>
curl -X POST -d "{\"operation\":\"create\",\"tableName\":\"$TABLENAME\",\"payload\":\
{\"Item\":{\"id\":\"2\",\"name\":\"Wong\"}}}" $URL
```
